#VPC
resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames = true
}

#IGW
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

#Public subnets
resource "aws_subnet" "public" {
  count                   = length(var.public_subnet_cidr_block)
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_subnet_cidr_block[count.index]
  availability_zone       = var.azs[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "Public-Subnet-${count.index}"
    "kubernetes.io/cluster/tx" = "shared"
    "kubernetes.io/role/elb"              = 1
  }
}

#Private subnets
resource "aws_subnet" "private" {
  count             = length(var.private_subnet_cidr_block)
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.private_subnet_cidr_block[count.index]
  availability_zone = var.azs[count.index]

  tags = {
    Name = "Private-Subnet-${count.index}"
    "kubernetes.io/cluster/tx" = "shared"
    "kubernetes.io/role/internal-elb"     = 1
  }
}

#Public route table
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
    Name = "Public-Route-Table"
  }
}

#Associate public route table
resource "aws_route_table_association" "public" {
  count          = length(var.public_subnet_cidr_block)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}

#NAT gateway
resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.main.id
  subnet_id     = aws_subnet.public[0].id

  tags = {
    Name = "NAT-Gateway"
  }
}

#EIP for NAT gateway
resource "aws_eip" "main" {
  vpc = true
}

#Private route table
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main.id
  }

  tags = {
    Name = "Private-Route-Table"
  }
}

#Associate private route table
resource "aws_route_table_association" "private" {
  count          = length(var.private_subnet_cidr_block)
  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private.id
}