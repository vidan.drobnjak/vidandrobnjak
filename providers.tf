terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0"
    }
  }

  backend "s3" {
    bucket         	   = "txinfra"
    key              	 = "state/terraform.tfstate"
    region         	   = "us-east-1"
    #encrypt        	   = true
    #dynamodb_table = "txinfra-state-lock"
  }
}

provider "aws" {
  region = "us-east-1"
}

provider "helm" {
  kubernetes {
    host                   = aws_eks_cluster.tx.endpoint
    cluster_ca_certificate = base64decode(aws_eks_cluster.tx.certificate_authority.0.data)
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      args        = ["eks", "get-token", "--cluster-name", aws_eks_cluster.tx.name]
      command     = "aws"
    }
  }
}