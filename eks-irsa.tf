# Cert Manager IRSA
module "cert_manager_irsa_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"

  role_name                     = "cert-manager"
  attach_cert_manager_policy    = true
  cert_manager_hosted_zone_arns = ["arn:aws:route53:::hostedzone/Z04750561MLX6GSP1D7PF"] # Your Route53 domain HostedZone


  oidc_providers = {
    eks = {
      provider_arn               = "arn:aws:iam::029675209931:oidc-provider/oidc.eks.us-east-1.amazonaws.com/id/7A57AA82F03F24ACD8A13C18E84B7A06"
      namespace_service_accounts = ["kube-system:cert-manager"]
    }
  }

}


# External DNS IRSA
#module "external_dns_irsa_role" {
#  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
#  version = "5.2.0" # Latest as of July 2022
#  role_name                     = "external-dns"
#  attach_external_dns_policy    = true
#  external_dns_hosted_zone_arns = ["arn:aws:route53:::hostedzone/Z04750561MLX6GSP1D7PF"]
#  oidc_providers = {
#    eks = {
#      provider_arn               = "arn:aws:iam::029675209931:oidc-provider/oidc.eks.us-east-1.amazonaws.com/#id/7A57AA82F03F24ACD8A13C18E84B7A06"
#      namespace_service_accounts = ["kube-system:external-dns"]
#    }
#  }
#}