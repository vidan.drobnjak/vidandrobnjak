#EKS
output "cluster_name" {
  value = aws_eks_cluster.tx.name
}


output "endpoint" {
  value = aws_eks_cluster.tx.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.tx.certificate_authority[0].data
}

output "eks_cluster_identity_oidc_issuer_arn" {
  value       = aws_eks_cluster.tx.identity[0].oidc[0].issuer
}